const db = require('./db')
const Query = {

    //resolver function for products returns list
    products: () => db.products.list(),

    productById: (root, args, context, info) => {
        //args will contain parameter passed in query
        return db.products.get(args.id);
    }
}

module.exports = { Query }