import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { ApolloClient, InMemoryCache, gql } from '@apollo/client'

export default function Home({ products }) {
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to <a href="https://nextjs.org">Next.js!</a> GraphQL
    </h1>

        <p className={styles.description}>
          Get started by editing{' '}
          <code className={styles.code}>pages/index.js</code>
        </p>

        <div className={styles.grid}>
          {products.map(launch => {
            return (
              <a key={launch.id} href={launch.name} className={styles.card}>
                <h3>{launch.name}</h3>
                <p>{launch.description}</p>
                <p><strong>{launch.promo_price}</strong></p>
              </a>
            );
          })}
        </div>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}

export async function getStaticProps() {
  const client = new ApolloClient({
    uri: process.env.urlgraphql,
    cache: new InMemoryCache()
  });
  const { data } = await client.query({
    query: gql`
    query{
      products{
        id
        name
        sku
        price
        description
        is_displayed
        start_promo
        end_promo
        promo_price
        gender
      }
    }`
  });
  return {
    props: {
      products: data.products
    }
  }
}
